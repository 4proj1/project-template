<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpClient\HttpClient;
use Psr\Http\Message\ResponseInterface;

class NotificationController extends AbstractController
{
	private static $account = array();

    public function getAccountByUser($username){
        return self::$account[$this->$username];
    }

    public function getAllAccount(){
        return self::$account;
    }

    public static function addAccount ($username, $ip) {
    	for($i = 0; $i < count(self::$account); $i++) {
	    	if(array_key_exists($username, self::$account)) {
	    		for($i2 = 0; $i2 < count(self::$account[$username]); $i2++) {
	    			if(self::$account[$username][$i2] != $ip) {
	    				array_push(self::$account[$username], $ip);
	    				return false;
	    			}
	    		}
	    	}
    	}
    	self::$account[$username] = array($ip);
		return true;
    }

	public function webService($username, $type, $message) {
    	for($i = 0; $i < count(self::$account); $i++) {
	    	if(array_key_exists($username, self::$account)) {
	    		for($i2 = 0; $i2 < count(self::$account[$username]); $i2++) {
	    			$theIP = self::$account[$username][$i2];
	    			$httpClientUser = HttpClient::create();
					$requestIP = $httpClientUser->request('GET', "http://". $theIP. "?type=". $type. "&message=". $message);
					var_dump($requestIP);
	    		}
	    		break;
	    	}
    	}
    	return $this->getMap();
    }

	public function getMap() {

    	$_SERVER["URL_MAP"] = 'http://10.8.0.5:8000/getMap';

    	$mapURL = $_SERVER["URL_MAP"];

		$httpClient = HttpClient::create();
        $response = $httpClient->request('GET', $mapURL);
		/*
        $statusCode = $response->getStatusCode();
        echo $statusCode . "\n";

        $content = $response->getContent();
        var_dump($content);
        echo $content . "\n";
        */
    }

    /**
     * @Route("/notification/{username}--{type}--{message}", name="notification", methods={"GET","HEAD"})
     */
    public function index(string $username, string $type, string $message)
    {
    	$this->addAccount("toto", "127.0.0.1");

    	$this->addAccount("thomas", "192.168.10.2");

    	$this->addAccount("toto", "127.0.0.1");

    	self::webService($username, $type, $message);

        return $this->render('notification/index.html.twig', [
            'controller_name' => 'NotificationController',
        ]);
    }
}
